#Instructions for human
name = input("Hi! What is your name?")


#Generated a random number
from random import randint

for guess in range(1,6):
    print(f"Guess {guess}: {name}, were you born in {randint(1,12)} / {randint(1924,2004)}")
    human_response = input("yes or no?: ")

    if human_response == "yes":
        print("I knew it!")
        break
    elif guess == 5:
        print("I have better things to do. Goodbye.")
    else:
        print("Drat! Lemme try again!")
